# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2021-01-26)


### Features

* add commitlint ([b023a79](https://gitlab.com/zw-slime/umi-app-test/commit/b023a79abaa56e322f82dff10734a53b2a5b87cc))
